/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   add_tpool_ttask.c                                  :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/07 16:42:21 by pholster       #+#    #+#                */
/*   Updated: 2020/02/07 16:42:21 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_thpool.h"

t_ttask		*add_tpool_ttask(t_tpool *pool, t_ttask *task)
{
	if (task == NULL || (pool->flags & TFLAG_POOL_TERMINATE) != 0)
		return (NULL);
	if (pool->alloced == FALSE && (pool->flags & TFLAG_POOL_ALLOC_ON_EXEC) != 0)
	{
		if (alloc_tpool_tthreads(&pool) == FALSE)
			return (NULL);
	}
	return (add_tqueue_ttask(pool->tasks, task));
}
