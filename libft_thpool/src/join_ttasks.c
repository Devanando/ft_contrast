/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   join_ttasks.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/07 16:39:56 by pholster       #+#    #+#                */
/*   Updated: 2020/02/07 16:39:56 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_thpool.h"

void	join_ttasks(t_ttask **tasks, size_t len)
{
	size_t	i;

	i = 0;
	if (tasks == NULL)
		return ;
	while (i < len)
	{
		join_ttask(tasks[i]);
		i++;
	}
}
