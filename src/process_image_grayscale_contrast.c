/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   process_image_grayscale_contrast.c                 :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/02 19:07:41 by pholster       #+#    #+#                */
/*   Updated: 2020/02/02 19:07:41 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <pthread.h>

#include "libft/libft.h"
#include "libft/ft_threadpool.h"
#include "libft_thpool/ft_thpool.h"

#include "ft_contrast.h"

static void	*process_contrast(t_imagestream *image, size_t start, size_t end)
{
	size_t	i;
	int		value;
	float	factor;

	i = start;
	factor = image->max_scale + 4;
	factor = (float)(factor * (image->con_level + image->max_scale)) / \
	(image->max_scale * (factor - image->con_level));
	while (i < end)
	{
		value = (image->max_scale / 2) + (image->max_scale % 2);
		value = factor * (image->bitmap[i].pgm.scale - value) + value;
		image->bitmap[i].pgm.scale = ft_constrain(value, 0, image->max_scale);
		i++;
	}
	return (NULL);
}

static void	process_contrast_multi_thread(t_imagestream *image)
{
	size_t		i;
	size_t		thread_bound;
	pthread_t	threads[MULTI_THREAD_COUNT];
	size_t		start;
	size_t		end;

	i = 0;
	thread_bound = image->bitmap_size / MULTI_THREAD_COUNT;
	while (i < MULTI_THREAD_COUNT)
	{
		start = thread_bound * i;
		end = start + thread_bound;
		if ((i + 1) == MULTI_THREAD_COUNT)
			end = image->bitmap_size;
		run_thread(&threads[i], new_ttask(process_contrast, 0, 3, image, start, end));
		i++;
	}
	while (i > 0)
	{
		i--;
		pthread_join(threads[i], NULL);
	}
}

static void	process_contrast_thread_pool(t_imagestream *image)
{
	size_t		i;
	size_t		thread_bound;
	size_t		start;
	size_t		end;

	i = 0;
	thread_bound = image->bitmap_size / MULTI_THREAD_COUNT;
	while (i < MULTI_THREAD_COUNT)
	{
		start = thread_bound * i;
		end = start + thread_bound;
		if ((i + 1) == MULTI_THREAD_COUNT)
			end = image->bitmap_size;
		add_tpool_ttask(image->tpool,
			new_ttask(process_contrast, 0, 3, image, start, end));
		i++;
	}
	join_tpool(image->tpool);
}

void		process_image_grayscale_contrast(t_imagestream *image)
{
	if (image->mode == SINGLE_THREAD)
		process_contrast(image, 0, image->bitmap_size);
	else if (image->mode == MULTI_THREAD)
		process_contrast_multi_thread(image);
	else if (image->mode == THREAD_POOL)
		process_contrast_thread_pool(image);
}
