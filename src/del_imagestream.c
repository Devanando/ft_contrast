/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   del_imagestream.c                                  :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/01/31 22:18:59 by pholster       #+#    #+#                */
/*   Updated: 2020/01/31 22:18:59 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft/ft_mem.h"
#include "libft_thpool/ft_thpool.h"

#include "ft_contrast.h"

void	del_imagestream(t_imagestream *image)
{
	if (image == NULL)
		return ;
	ft_memdel((void**)image->bitmap);
	if (image->tpool != NULL)
		del_tpool(&image->tpool);
}
