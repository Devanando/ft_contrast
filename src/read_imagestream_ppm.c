/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   read_imagestream_ppm.c                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/01 11:09:37 by pholster       #+#    #+#                */
/*   Updated: 2020/02/01 11:09:37 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_contrast.h"

t_bool		read_imagestream_ppm(t_imagestream *image, int fd)
{
	(void)image;
	(void)fd;
	return (FALSE);
}
