/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   convert_value.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/02 21:52:13 by pholster       #+#    #+#                */
/*   Updated: 2020/02/02 21:52:13 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft/libft.h"

#include "ft_contrast.h"

size_t	convert_value(char *dst, size_t scale)
{
	size_t	i;
	size_t	len;

	len = ft_numlen(scale);
	i = len;
	while (i > 0)
	{
		i--;
		dst[i] = (scale % 10) + '0';
		scale /= 10;
	}
	return (len);
}
