/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   write_imagestream_pgm.c                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/01 16:43:09 by dkroeke        #+#    #+#                */
/*   Updated: 2020/02/01 17:31:17 by dkroeke       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "ft_contrast.h"

#include "libft/libft.h"

#define HEADER_WHITE_SPACE 6

static size_t	get_pgm_len(t_imagestream *image, size_t start, size_t end)
{
	size_t	len;
	size_t	i;

	i = start;
	len = ft_numlen(image->width) + ft_numlen(image->height) +
		ft_numlen(image->max_scale) + HEADER_WHITE_SPACE;
	while (i < end)
	{
		len += ft_numlen(image->bitmap[i].pgm.scale) + 1;
		i++;
	}
	return (len);
}

static void		convert_image_bitmap(t_imagestream *image, char *image_data,
										size_t start, size_t end)
{
	size_t	pixel;
	size_t	i;

	i = 0;
	pixel = start;
	while (pixel < end)
	{
		i += convert_value(&image_data[i], image->bitmap[pixel].pgm.scale);
		image_data[i] = ' ';
		pixel++;
		i++;
	}
}

static size_t	convert_image_header(t_imagestream *image, char *image_data)
{
	size_t	i;

	i = 3;
	ft_strcpy(image_data, "P2\n");
	i += convert_value(&image_data[i], image->width);
	image_data[i] = ' ';
	i++;
	i += convert_value(&image_data[i], image->height);
	image_data[i] = '\n';
	i++;
	i += convert_value(&image_data[i], image->max_scale);
	image_data[i] = '\n';
	i++;
	return (i);
}

t_bool			write_imagestream_pgm(t_imagestream *image, int fd_image)
{
	char	*image_data;
	size_t	pgm_len;
	size_t	header_len;

	pgm_len = get_pgm_len(image, 0, image->bitmap_size);
	image_data = ft_memalloc(pgm_len);
	if (image_data == 0)
		return (FALSE);
	header_len = convert_image_header(image, image_data);
	convert_image_bitmap(image, &image_data[header_len], 0, image->bitmap_size);
	write(fd_image, image_data, pgm_len);
	ft_strdel(&image_data);
	return (TRUE);
}
