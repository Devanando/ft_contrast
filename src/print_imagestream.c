/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   print_imagestream.c                                :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/01 10:23:46 by pholster       #+#    #+#                */
/*   Updated: 2020/02/01 10:23:46 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft/ft_str.h"
#include "libft/ft_printf.h"

#include "ft_contrast.h"

void	print_imagestream(t_imagestream *image)
{
	if (image == NULL)
	{
		ft_putstr(NULL);
		return ;
	}
	ft_printf("Image: %s -> %s\nTarget contrast: %d\nType: %d\n\
Human Readable: %d\nMaxScale: %hu\nDimensions: (%zu, %zu)\nBitmap size: %zu\n",
	image->input, image->output, image->con_level, image->type,
	image->human_readable, image->max_scale, image->width, image->height,
	image->bitmap_size);
}
