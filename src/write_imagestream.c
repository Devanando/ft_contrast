/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   write_imagestream.c                                :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/01 16:43:09 by dkroeke        #+#    #+#                */
/*   Updated: 2020/02/01 17:31:17 by dkroeke       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "ft_contrast.h"

t_bool			write_imagestream(t_imagestream *image)
{
	int			fd_image;
	t_bool		failed;

	failed = FALSE;
	fd_image = open(image->output, O_WRONLY | O_CREAT | O_TRUNC, 0777);
	if (fd_image == -1)
		return (FALSE);
	if (image->type == PGM)
		failed = write_imagestream_pgm(image, fd_image);
	close(fd_image);
	return (failed);
}
