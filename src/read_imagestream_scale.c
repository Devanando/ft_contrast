/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   read_imagestream_scale.c                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/01 10:46:47 by pholster       #+#    #+#                */
/*   Updated: 2020/02/01 10:46:47 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft/ft_num.h"
#include "libft/ft_str.h"
#include "libft/ft_strarr.h"

#include "ft_contrast.h"

static t_bool	validate_scale(t_imagestream *image, char *scale)
{
	size_t		biggest_scale_len;
	intmax_t	scale_num;

	scale_num = ft_atoi(scale);
	biggest_scale_len = ft_max(PGM_MAX_SCALE_LEN, PPM_MAX_SCALE_LEN);
	if (ft_strlen(scale) > biggest_scale_len || scale_num <= 0)
		return (FALSE);
	if (image->type == PGM && scale_num > PGM_MAX_SCALE)
		return (FALSE);
	if (image->type == PPM && scale_num > PPM_MAX_SCALE)
		return (FALSE);
	return (TRUE);
}

static t_bool	parse_scale(t_imagestream *image, char **fields)
{
	size_t	index;
	size_t	fields_len;

	index = 0;
	fields_len = ft_strarrlen((const char **)fields);
	while (index < fields_len)
	{
		if (fields[index][0] == '#')
			break ;
		if (index == 0)
		{
			if (validate_scale(image, fields[index]) == FALSE)
				return (FALSE);
			image->max_scale = ft_atoi(fields[index]);
		}
		else
			return (FALSE);
		index++;
	}
	return (TRUE);
}

t_bool			read_imagestream_scale(t_imagestream *image, int fd)
{
	char	*line;
	char	**fields;
	t_bool	parse_succeeded;

	if (read_imagestream_line(fd, &line) <= 0)
		return (handle_error(image, FAIL_IMG_SCALE, TRUE));
	fields = ft_strsplit(line, ' ');
	if (fields == NULL)
		return (handle_error(image, FAIL_IMG_SCALE, TRUE));
	ft_strdel(&line);
	parse_succeeded = parse_scale(image, fields);
	ft_strarrdel(&fields);
	if (parse_succeeded == FALSE || image->max_scale <= 0)
		return (handle_error(image, INV_IMG_SCALE, TRUE));
	return (TRUE);
}
