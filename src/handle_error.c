/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   handle_error.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/01 12:23:01 by dkroeke        #+#    #+#                */
/*   Updated: 2020/02/01 13:16:46 by dkroeke       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_contrast.h"
#include "libft/libft.h"

int			handle_error(t_imagestream *image, t_errors err, t_bool bool_ret)
{
	if (err == USAGE)
		ft_dprintf(2, "%s\n", g_error_message[err]);
	else if (err < INV_IMG_DIM)
		ft_dprintf(2, "Invalid: %s\n", g_error_message[err]);
	else
		ft_dprintf(2, "Malloc error: %s\n", g_error_message[err]);
	if (err > INV_CON_VAL && err != FAIL_TP)
		del_imagestream(image);
	if (bool_ret == TRUE)
		return (FALSE);
	return (TRUE);
}
