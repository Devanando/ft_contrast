/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   process_image_grayscale.c                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/01 11:42:51 by dkroeke        #+#    #+#                */
/*   Updated: 2020/02/02 13:43:30 by dkroeke       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_contrast.h"

void		process_image_grayscale(t_imagestream *image)
{
	if (image->change_brightness == TRUE)
		process_image_grayscale_brightness(image);
	else
		process_image_grayscale_contrast(image);
}
