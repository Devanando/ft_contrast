/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   read_imagestream.c                                 :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/01/31 22:24:44 by pholster       #+#    #+#                */
/*   Updated: 2020/01/31 22:24:44 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>

#include "libft/ft_bool.h"
#include "libft/ft_str.h"
#include "libft/ft_mem.h"

#include "ft_contrast.h"

static t_bool	parse_file_format(t_imagestream *image, char *line)
{
	t_bool	type_found;

	type_found = FALSE;
	if (ft_strequ(line, "P2") || ft_strequ(line, "P5"))
	{
		image->type = PGM;
		type_found = TRUE;
	}
	if (ft_strequ(line, "P3") || ft_strequ(line, "P6"))
	{
		image->type = PPM;
		type_found = TRUE;
	}
	if (type_found)
		image->human_readable = (line[1] < '4');
	return (type_found);
}

static t_bool	read_file_format(t_imagestream *image, int fd)
{
	char	*line;
	ssize_t	line_len;
	t_bool	type_found;

	line_len = ft_getnextline(fd, &line);
	if (line_len != 3)
	{
		ft_strdel(&line);
		return (handle_error(image, INV_IMG_TYPE, TRUE));
	}
	type_found = parse_file_format(image, line);
	ft_strdel(&line);
	if (type_found == FALSE)
		return (handle_error(image, INV_IMG_TYPE, TRUE));
	return (type_found);
}

static t_bool	read_general_info(t_imagestream *image, int fd)
{
	if (read_file_format(image, fd) == FALSE)
		return (FALSE);
	if (read_imagestream_dimensions(image, fd) == FALSE)
		return (FALSE);
	if (read_imagestream_scale(image, fd) == FALSE)
		return (FALSE);
	image->bitmap_size = image->width * image->height;
	image->bitmap = ft_memalloc(sizeof(t_mapvalue) * image->bitmap_size);
	if (image->bitmap == NULL)
		return (handle_error(image, FAIL_IMG_BITMAP, TRUE));
	return (TRUE);
}

static t_bool	read_image_pixels(t_imagestream *image, int fd)
{
	if (image->type == PGM)
		return (read_imagestream_pgm(image, fd));
	else if (image->type == PPM)
		return (read_imagestream_ppm(image, fd));
	return (FALSE);
}

t_bool			read_imagestream(t_imagestream *image)
{
	int		fd;
	t_bool	valid;

	fd = open(image->input, O_RDONLY);
	if (fd < 0)
		return (handle_error(image, INV_IMG_FORMAT, TRUE));
	if (read_general_info(image, fd) == FALSE)
	{
		close(fd);
		return (FALSE);
	}
	valid = read_image_pixels(image, fd);
	close(fd);
	if (valid == FALSE)
		return (handle_error(image, INV_IMG_TYPE, TRUE));
	return (valid);
}
