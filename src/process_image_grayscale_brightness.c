/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   process_image_grayscale_brightness.c               :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/02 19:07:41 by pholster       #+#    #+#                */
/*   Updated: 2020/02/02 19:07:41 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <pthread.h>

#include "libft/libft.h"
#include "libft/ft_threadpool.h"

#include "ft_contrast.h"

static void	*process_brightness(t_imagestream *image, size_t start, size_t end)
{
	size_t	i;
	float	factor;
	int		value;

	i = start;
	factor = ((float)image->con_level / 100) + 1;
	while (i < end)
	{
		value = image->bitmap[i].pgm.scale * factor;
		image->bitmap[i].pgm.scale = ft_constrain(value, 0, image->max_scale);
		i++;
	}
	return (NULL);
}

static void	process_brightness_multi_thread(t_imagestream *image)
{
	size_t		i;
	size_t		thread_bound;
	pthread_t	threads[MULTI_THREAD_COUNT];
	size_t		start;
	size_t		end;

	i = 0;
	thread_bound = image->bitmap_size / MULTI_THREAD_COUNT;
	while (i < MULTI_THREAD_COUNT)
	{
		start = thread_bound * i;
		end = start + thread_bound;
		if ((i + 1) == MULTI_THREAD_COUNT)
			end = image->bitmap_size;
		run_thread(&threads[i], new_ttask(process_brightness, 0, 3, image, start, end));
		i++;
	}
	while (i > 0)
	{
		i--;
		pthread_join(threads[i], NULL);
	}
}

void		process_image_grayscale_brightness(t_imagestream *image)
{
	if (image->mode == SINGLE_THREAD)
		process_brightness(image, 0, image->bitmap_size);
	else if (image->mode == MULTI_THREAD)
		process_brightness_multi_thread(image);
	else if (image->mode == THREAD_POOL)
		return ;
}
