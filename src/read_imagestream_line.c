/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   read_imagestream_line.c                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/01 10:15:56 by pholster       #+#    #+#                */
/*   Updated: 2020/02/01 10:15:56 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft/ft_char.h"
#include "libft/ft_str.h"

static t_bool	is_comment_line(char *line)
{
	size_t	blank_len;

	blank_len = ft_strislen(line, &ft_isblank);
	return (line[blank_len] == '#');
}

ssize_t			read_imagestream_line(int fd, char **line)
{
	ssize_t	line_len;

	*line = NULL;
	while (*line == NULL || is_comment_line(*line))
	{
		ft_strdel(line);
		line_len = ft_getnextline(fd, line);
		if (line_len == -1 || *line == NULL)
		{
			ft_strdel(line);
			return (-1);
		}
	}
	return (line_len);
}
