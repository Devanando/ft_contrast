/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   main_tp.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/01/31 20:53:07 by pholster       #+#    #+#                */
/*   Updated: 2020/02/02 13:34:40 by dkroeke       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft/libft.h"
#include "libft_thpool/ft_thpool.h"

#include "ft_contrast.h"

int				main(int argc, char **argv)
{
	t_imagestream	image;

	ft_bzero(&image, sizeof(image));
	image.mode = THREAD_POOL;
	image.tpool = new_tpool(POOL_THREAD_COUNT, 0);
	if (image.tpool == NULL)
		return (handle_error(&image, FAIL_TP, FALSE));
	return (start(&image, argc, argv));
}
