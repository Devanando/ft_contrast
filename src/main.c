/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   main.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/01/31 20:53:07 by pholster       #+#    #+#                */
/*   Updated: 2020/02/02 13:34:40 by dkroeke       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft/libft.h"

#include "ft_contrast.h"

int				main(int argc, char **argv)
{
	t_imagestream	image;

	ft_bzero(&image, sizeof(image));
	image.mode = SINGLE_THREAD;
	return (start(&image, argc, argv));
}
