/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   start.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/01/31 20:53:07 by pholster       #+#    #+#                */
/*   Updated: 2020/02/02 13:34:40 by dkroeke       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft/libft.h"
#include "ft_contrast.h"

int				parse_con_level(char **argv, t_imagestream *image, int i)
{
	if (argv[i][1] == 'c' && argv[i + 1] != NULL)
	{
		image->con_level = (int)ft_atoi(argv[i + 1]);
		if ((image->con_level == 0) && (argv[i + 1][0] != '0'))
			return (handle_error(image, INV_CON, FALSE));
		if (image->con_level < -100 || image->con_level > 100)
			return (handle_error(image, INV_CON_VAL, FALSE));
	}
	if (argv[i][1] == 'b')
		image->change_brightness = 1;
	return (0);
}

int				parse_arg(char **argv, t_imagestream *image)
{
	int i;

	i = 0;
	if (!image)
		return (1);
	while (argv[i])
	{
		if (i % 2 != 0)
		{
			if (argv[i][0] != '-' && argv[i][2] != '\0')
				return (handle_error(image, INV_OPT, FALSE));
			if (argv[i][1] == 'f' && argv[i + 1] != NULL)
				image->input = argv[i + 1];
			if (argv[i][1] == 'o' && argv[i + 1] != NULL)
				image->output = argv[i + 1];
			if (parse_con_level(argv, image, i) != 0)
				return (1);
		}
		i++;
	}
	return (0);
}

int				start(t_imagestream *image, int argc, char **argv)
{
	if (argc < 7 || argc > 8)
		return (handle_error(image, USAGE, FALSE));
	if (parse_arg(argv, image) != 0)
		return (0);
	if (image->input == NULL || image->input == NULL || image->con_level == -1)
		return (handle_error(image, INV_ARG, FALSE));
	if (read_imagestream(image) == FALSE)
		return (handle_error(image, INV_IMG, FALSE));
	print_imagestream(image);
	process_image_grayscale(image);
	if (write_imagestream(image) == FALSE)
		return (handle_error(image, FAIL_IMG_WRITE_FILE, FALSE));
	del_tpool(&image->tpool);
	return (0);
}
