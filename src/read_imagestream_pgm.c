/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   read_imagestream_pgm.c                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/01 11:09:37 by pholster       #+#    #+#                */
/*   Updated: 2020/02/01 11:09:37 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft/ft_mem.h"
#include "libft/ft_num.h"
#include "libft/ft_char.h"
#include "libft/ft_str.h"
#include "libft/ft_strarr.h"

#include "ft_contrast.h"

static t_bool	parse_line_binary(t_imagestream *image, size_t *bitmap_i,
									char *line, size_t line_len)
{
	size_t		i;
	size_t		pixel_size;
	t_uint32	pixel_mask;
	t_uint16	value;

	i = 0;
	pixel_size = sizeof(t_uint8);
	if (image->max_scale > PGM_8BIT_SCALE)
		pixel_size = sizeof(t_uint16);
	pixel_mask = ft_pow(0x000000FF, pixel_size);
	while (i < line_len)
	{
		if (*bitmap_i > image->bitmap_size)
			return (handle_error(image, INV_IMG_INDEX, TRUE));
		value = line[i] & pixel_mask;
		if (value < 0 || value > image->max_scale)
			return (handle_error(image, INV_IMG_PIXEL, TRUE));
		image->bitmap[*bitmap_i].pgm.scale = value;
		*bitmap_i += 1;
		i += pixel_size;
	}
	return (TRUE);
}

static t_bool	parse_line_human(t_imagestream *image, size_t *bitmap_i,
									char *line)
{
	size_t		i;
	t_uint16	value;

	i = 0;
	while (line[i] != '\0')
	{
		if (line[i] != ' ')
		{
			if (*bitmap_i > image->bitmap_size)
				return (handle_error(image, INV_IMG_INDEX, TRUE));
			value = (t_uint16)ft_atoi(&line[i]);
			if (value < 0 || value > image->max_scale)
				return (handle_error(image, INV_IMG_PIXEL, TRUE));
			image->bitmap[*bitmap_i].pgm.scale = value;
			i += ft_strislen(&line[i], ft_isdigit);
			*bitmap_i += 1;
		}
		else
			i++;
	}
	return (TRUE);
}

t_bool			read_imagestream_pgm(t_imagestream *image, int fd)
{
	size_t	total;
	ssize_t	line_len;
	t_bool	valid_line;
	char	*line;

	total = 0;
	line_len = ft_getnextline(fd, &line);
	while (line_len > 0)
	{
		if (image->human_readable)
			valid_line = parse_line_human(image, &total, line);
		else
			valid_line = parse_line_binary(image, &total, line, line_len);
		ft_strdel(&line);
		if (valid_line == FALSE)
			return (FALSE);
		line_len = ft_getnextline(fd, &line);
	}
	ft_strdel(&line);
	if (line_len == -1)
		return (handle_error(image, FAIL_IMG_READ_PGM, TRUE));
	if (total != image->bitmap_size)
		return (handle_error(image, INV_IMG_PGM, TRUE));
	return (TRUE);
}
