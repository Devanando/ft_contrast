/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   read_imagestream_dimensions.c                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/01 10:43:13 by pholster       #+#    #+#                */
/*   Updated: 2020/02/01 10:43:13 by pholster      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft/ft_num.h"
#include "libft/ft_str.h"
#include "libft/ft_strarr.h"

#include "ft_contrast.h"

static t_bool	parse_size(t_imagestream *image, char **fields)
{
	size_t	index;
	size_t	fields_len;

	index = 0;
	fields_len = ft_strarrlen((const char **)fields);
	while (index < fields_len)
	{
		if (fields[index][0] == '#')
			break ;
		if (index == 0)
			image->width = ft_atoi(fields[index]);
		else if (index == 1)
			image->height = ft_atoi(fields[index]);
		else
			return (FALSE);
		index++;
	}
	return (TRUE);
}

t_bool			read_imagestream_dimensions(t_imagestream *image, int fd)
{
	char	*line;
	char	**fields;
	t_bool	parse_succeeded;

	if (read_imagestream_line(fd, &line) <= 0)
		return (handle_error(image, FAIL_IMG_DIM, TRUE));
	fields = ft_strsplit(line, ' ');
	if (fields == NULL)
		return (handle_error(image, FAIL_IMG_DIM, TRUE));
	ft_strdel(&line);
	parse_succeeded = parse_size(image, fields);
	ft_strarrdel(&fields);
	if (parse_succeeded == FALSE || image->width <= 0 || image->height <= 0)
		return (handle_error(image, INV_IMG_DIM, TRUE));
	return (TRUE);
}
