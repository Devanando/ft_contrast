/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_contrast.h                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/01/31 21:03:22 by dkroeke        #+#    #+#                */
/*   Updated: 2020/02/01 17:32:23 by dkroeke       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_CONTRAST_H
# define FT_CONTRAST_H

# include "libft/ft_color.h"
# include "libft/ft_types.h"
# include "libft_thpool/ft_thpool.h"

# define PGM_8BIT_SCALE		255
# define PGM_MAX_SCALE		65535
# define PGM_MAX_SCALE_LEN	5

# define PPM_MAX_SCALE		255
# define PPM_MAX_SCALE_LEN	3

# define MULTI_THREAD_COUNT	4
# define POOL_THREAD_COUNT	4

typedef enum		e_errors
{
	USAGE,
	INV_CON,
	INV_CON_VAL,
	INV_OPT,
	INV_ARG,
	INV_IMG,
	INV_IMG_FORMAT,
	INV_IMG_TYPE,
	INV_IMG_SCALE,
	INV_IMG_INDEX,
	INV_IMG_PIXEL,
	INV_IMG_PGM,
	INV_IMG_DIM,
	FAIL_IMG_DIM,
	FAIL_IMG_SCALE,
	FAIL_IMG_READ_PGM,
	FAIL_IMG_WRITE_FILE,
	FAIL_IMG_BITMAP,
	FAIL_TP,
	ERROR_COUNT
}					t_errors;

static const char *g_error_message[ERROR_COUNT] = {
	"Usage : ./ft_contrast [option] [input]\noptions\t\tinput \
	\n-o\t\tOutput stream\n-f\t\tInput file\n-c\t\tContrast level {-100, 100} \
	\n-b\t\tNo input required, change brightness instead of contrast",
	"contrast input",
	"contrast value",
	"option",
	"argument",
	"input image",
	"image format",
	"image type",
	"image scale",
	"image pixel index",
	"image pixel value",
	"image pgm size",
	"image dimensions",
	"read image dimensions",
	"read image scale",
	"read image pgm pixels",
	"write image file",
	"image bitmap",
	"pool allocation",
};

typedef enum		e_mode
{
	SINGLE_THREAD,
	MULTI_THREAD,
	THREAD_POOL,
}					t_mode;

typedef enum		e_map
{
	PBM,
	PGM,
	PPM,
}					t_map;

typedef struct		s_grayscale
{
	t_uint16		scale;
}					t_grayscale;

typedef union		u_mapvalue
{
	t_color			ppm;
	t_grayscale		pgm;
}					t_mapvalue;

typedef struct		s_imagestream
{
	t_mode			mode;
	char			*input;
	char			*output;
	int				con_level;
	t_map			type;
	t_bool			human_readable;
	t_bool			change_brightness;
	t_uint16		max_scale;
	size_t			width;
	size_t			height;
	size_t			bitmap_size;
	t_mapvalue		*bitmap;
	t_tpool			*tpool;
}					t_imagestream;

int					start(t_imagestream *image, int argc, char **argv);
size_t				convert_value(char *dst, size_t scale);
ssize_t				read_imagestream_line(int fd, char **line);
t_bool				read_imagestream_pgm(t_imagestream *image, int fd);
t_bool				read_imagestream_ppm(t_imagestream *image, int fd);
t_bool				read_imagestream_scale(t_imagestream *image, int fd);
t_bool				read_imagestream_dimensions(t_imagestream *image, int fd);
t_bool				read_imagestream(t_imagestream *image);
void				del_imagestream(t_imagestream *image);
void				print_imagestream(t_imagestream *image);
void				process_image_grayscale(t_imagestream *image);
void				process_image_grayscale_contrast(t_imagestream *image);
void				process_image_grayscale_brightness(t_imagestream *image);
t_bool				write_imagestream(t_imagestream *image);
t_bool				write_imagestream_pgm(t_imagestream *image, int fd_image);
int					handle_error(t_imagestream *image, t_errors err,
									t_bool bool_ret);

#endif
