# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    makefile                                           :+:    :+:             #
#                                                      +:+                     #
#    By: pholster <pholster@student.codam.nl>         +#+                      #
#                                                    +#+                       #
#    Created: 2020/01/31 20:42:43 by pholster       #+#    #+#                 #
#    Updated: 2020/02/02 13:38:42 by dkroeke       ########   odam.nl          #
#                                                                              #
# **************************************************************************** #

NAME = ft_contrast
NAME_TH = ft_contrast_th
NAME_TP = ft_contrast_tp

# Compile settings
INCLUDES = includes
CC_STRICT = -Wall -Werror -Wextra

# Compile flags
CC_FLAGS = -g $(CC_STRICT) -I$(INCLUDES)
ifeq ($(shell uname -s), Linux)
CC_FLAGS += -pthread
endif

# Libft info
LIBFT_PATH = libft
LIBFT = $(LIBFT_PATH)/libft.a

# Libft_thpool info
LIBFT_THPOOL_PATH = libft_thpool
LIBFT_THPOOL = $(LIBFT_THPOOL_PATH)/libft_thpool.a

SRC_PATH = src

# General source files
SRC_GEN = \
	start \
	print_imagestream \
	del_imagestream \
	write_imagestream \
	write_imagestream_pgm \
	read_imagestream \
	read_imagestream_line \
	read_imagestream_dimensions \
	read_imagestream_scale \
	read_imagestream_pgm \
	read_imagestream_ppm \
	process_image_grayscale \
	process_image_grayscale_contrast \
	process_image_grayscale_brightness \
	convert_value \
	handle_error \

SRC_GEN := $(SRC_GEN:%=$(SRC_PATH)/%.c)
OBJ_GEN = $(SRC_GEN:.c=.o)

# Single thread contrast source files
MAIN = $(SRC_PATH)/main.c
OBJ = $(MAIN:.c=.o) $(OBJ_GEN)

# Multi thread contrast source files
MAIN_TH = $(SRC_PATH)/main_th.c
OBJ_TH = $(MAIN_TH:.c=.o) $(OBJ_GEN)

# Thread pool contrast source files
MAIN_TP = $(SRC_PATH)/main_tp.c
OBJ_TP = $(MAIN_TP:.c=.o) $(OBJ_GEN)

DEP = $(OBJ_GEN:.o=.d) $(MAIN:.c=.d) $(MAIN_TG:.c=.d) $(MAIN_TP:.c=.d)

# Make file print
BASE_NAME = $(NAME:%.a=%)
MAKEFILE_INCLUDES = $(INCLUDES)/libft
include $(MAKEFILE_INCLUDES)/Makefile.color

# Trash files
TEMP_FILES = $(SRC:.c=.c~)
CLEAN_FILES = $(OBJ) $(TEMP_FILES) $(DEP)

# Start of it all
all: $(LIBFT) $(LIBFT_THPOOL) $(OBJ_GEN) $(NAME) $(NAME_TH) $(NAME_TP)

# Single thread
$(NAME): $(LIBFT) $(LIBFT_THPOOL) $(OBJ)
	@rm -f $(NAME)
	@$(call FNC_PRINT_EQUAL,$(BASE_NAME),$(NAME))
	@gcc $(CC_FLAGS) -o $(NAME) $(OBJ) $(LIBFT) $(LIBFT_THPOOL)

# Multi thread
$(NAME_TH): $(LIBFT) $(LIBFT_THPOOL) $(OBJ_TH)
	@rm -f $(NAME_TH)
	@$(call FNC_PRINT_EQUAL,$(BASE_NAME),$(NAME_TH))
	@gcc $(CC_FLAGS) -o $(NAME_TH) $(OBJ_TH) $(LIBFT) $(LIBFT_THPOOL)

# Thread pool
$(NAME_TP): $(LIBFT) $(LIBFT_THPOOL) $(OBJ_TP)
	@rm -f $(NAME_TP)
	@$(call FNC_PRINT_EQUAL,$(BASE_NAME),$(NAME_TP))
	@gcc $(CC_FLAGS) -o $(NAME_TP) $(OBJ_TP) $(LIBFT) $(LIBFT_THPOOL)

$(LIBFT): FORCE
	@$(MAKE) -s -C $(LIBFT_PATH)

$(LIBFT_THPOOL): FORCE
	@$(MAKE) -s -C $(LIBFT_THPOOL_PATH)

# Include all existing header dependencies files
include $(wildcard $(DEP))

%.o: %.c
	@$(call FNC_PRINT_PLUS,$(BASE_NAME),$(@:$(SRC_PATH)/%.o=%))
	@rm -f $(<:.c=.o)
	@$(CC) -MMD $(CC_FLAGS) -o $(<:.c=.o) -c $<

clean:
	@$(MAKE) -s -C $(LIBFT_PATH) clean
ifneq ($(wildcard $(CLEAN_FILES)),)
	@$(call FNC_PRINT_MIN,$(BASE_NAME),clean)
	@rm -f $(CLEAN_FILES)
endif

fclean: clean
	@$(MAKE) -s -C $(LIBFT_PATH) fclean
	@$(call FNC_PRINT_DEL,$(BASE_NAME),fclean $(NAME))

re: fclean
	@$(MAKE)

FORCE: ;

.PHONY: all clean fclean re FORCE
