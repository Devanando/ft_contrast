/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   libft.h                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: pholster <pholster@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/01/08 16:12:36 by pholster       #+#    #+#                */
/*   Updated: 2020/01/31 23:01:51 by dkroeke       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LIBFT_H
# define FT_LIBFT_H

# include "ft_bool.h"
# include "ft_char.h"
# include "ft_color.h"
# include "ft_float.h"
# include "ft_hash.h"
# include "ft_list.h"
# include "ft_mem.h"
# include "ft_num.h"
# include "ft_numarr.h"
# include "ft_str.h"
# include "ft_strarr.h"
# include "ft_term.h"
# include "ft_unum.h"
# include "ft_utf8.h"
# include "ft_printf.h"

#endif
